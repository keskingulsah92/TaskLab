#!/bin/sh

ssh -o StrictHostKeyChecking=no ec2-user@13.58.238.145 << 'ENDSSH'
  cd /home/ec2-user/app
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull registry.gitlab.com/keskingulsah92/tasklab
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH