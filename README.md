# TaskLab
TaskLab helps teams move work forward.

Collaborate, manage projects, organize tasks, and build team spirit-all in one place. Manage projects, and 
reach new productivity peaks.

[Deployment Link](http://13.58.238.145)

## How to install and run the project for Mac users

```bash
$ git clone https://gitlab.com/keskingulsah92/TaskLab
$ cp .env.example .env
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```

## How to install and run the project for Windows users

```bash
$ git clone https://gitlab.com/keskingulsah92/TaskLab
$ cp .env.example .env
$ python -m venv venv
$ cd venv
$ .\Scripts\activate
$ cd ..
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```

## How to run on Docker

```bash
$ git clone https://gitlab.com/keskingulsah92/TaskLab
$ docker-compose up --build
```